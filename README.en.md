# MyCppAlgorithmAndDataStructure

#### Description
To realize the algorithm and data structure with Cpp

 ![1](https://img.shields.io/badge/license-Apache%202.0-blue.svg)

 ![ESIDE.CN](https://img.shields.io/badge/CN-ESIDE-orange.svg?longCache=true)

 ![v1.0.0](https://img.shields.io/badge/version-v1.0.0-red.svg?longCache=true)

#### Software Architecture
Software architecture description

+ include : place header files`.h`
+ src : palce source files`.cpp`
+ CMakeLists.txt : cmakefile

#### Installation

```
cmake .

make
```

#### Instructions

please check the ealgorithm

#### Contribution

MailTo: join@eside.cn

#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  LEOBOD Personal home page [www.leobod.cn](https://www.leobod.cn)
3.  ESIDE.CN Welcome all like-minded partners to communicate together [www.eside.cn](https://www.eside.cn)
