/**
 * equeue的测试
 *
 * equeue的使用案例与简单测试
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */


#include <stdio.h>
#include <stdlib.h>

#include "../include/equeue.h"


void queueTest() {
    ealgorithm::equeue::SequenceQueue queue;
    for (int i = 0; i < 5; ++i) {
        ealgorithm::equeue::enQueue(queue, i);
    }

    int recv;
    for (int i = 0; i < 5; ++i) {
        ealgorithm::equeue::deQueue(queue, recv);
        printf("pop the val from the stack %d \n", recv);
    }
    printf("\n");
}


int main(int argc, char *argv[]) {
    queueTest();
}
 
