/**
 * elist的测试
 *
 * elist的使用案例与简单测试
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */
 
#include <iostream>

#include "../include/elist.h"


// SequenceAList Test
void alistInitListTest() {
    ealgorithm::elist::SequenceAList L;
    ealgorithm::elist::initList(L);
    int capacity = sizeof(L.data) / sizeof(int);
    for (int i = 0; i < capacity; ++i) {
        std::cout << "L.data[" << i << "] = " << L.data[i] << std::endl;
    }
}

// SequencePList Test
void plistInitListTest() {
    ealgorithm::elist::SequencePList L;
    ealgorithm::elist::initList(L);
    int capacity = L.maxsize;
    for (int i = 0; i < capacity; ++i) {
        std::cout << "L.data[" << i << "] = " << L.data[i] << std::endl;
    }
}

int main(int argc, char *argv[]) {
    alistInitListTest();
    plistInitListTest();

    return 0;
}