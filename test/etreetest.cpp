/**
 * ${TITLE}
 * 
 * Content
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */


#include <stdio.h>
#include <stdlib.h>

#include "../include/etree.h"


int main(int argc, char *argv[]) {
    ealgorithm::etree::BiTree tree;
    ealgorithm::etree::BitNode *a, *b, *c, *d, *e, *f;

    tree = (ealgorithm::etree::BitNode *) malloc(sizeof(ealgorithm::etree::BitNode));
    a = (ealgorithm::etree::BitNode *) malloc(sizeof(ealgorithm::etree::BitNode));
    b = (ealgorithm::etree::BitNode *) malloc(sizeof(ealgorithm::etree::BitNode));
    c = (ealgorithm::etree::BitNode *) malloc(sizeof(ealgorithm::etree::BitNode));
    d = (ealgorithm::etree::BitNode *) malloc(sizeof(ealgorithm::etree::BitNode));
    e = (ealgorithm::etree::BitNode *) malloc(sizeof(ealgorithm::etree::BitNode));
    f = (ealgorithm::etree::BitNode *) malloc(sizeof(ealgorithm::etree::BitNode));
    ealgorithm::etree::initTree(tree);

    tree->data = 40;
    a->data = 30;   b->data = 50;
    c->data = 20;   d->data = 35;
    e->data = 10;
    f->data = 55;

    tree->lchild = a;
    tree->rchild = b;
    a->lchild = c;
    a->rchild = d;
    b->lchild = NULL;
    b->rchild = f;
    c->lchild = e;
    c->rchild = NULL;
    d->lchild = NULL;
    d->rchild = NULL;
    e->lchild = NULL;
    e->rchild = NULL;
    f->lchild = NULL;
    f->rchild = NULL;

    printf("depth is %d \n", ealgorithm::etree::treeDepth(tree));

    ealgorithm::etree::inOrder(tree);

    printf("\n");

    ealgorithm::etree::preOrder(tree);

    printf("\n");

    ealgorithm::etree::postOrder(tree);

    free(tree);
    free(a);
    free(b);
    free(c);
    free(d);
    free(e);
    free(f);

    return (0);
}


