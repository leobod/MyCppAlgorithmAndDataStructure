/**
 * ${TITLE}
 * 
 * Content
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */


#include <stdio.h>
#include <time.h>

#include "../include/ebag.h"
#include "../include/esort.h"


// 测试 冒泡排序
void testEsortBubble(int *arr, int len) {
    printf("before ... \n");
    ealgorithm::ebag::printAll(arr, len);
    printf("\n");

    printf("sorting ... \n");
    ealgorithm::esort::bubbleSort(arr, len);
    ealgorithm::ebag::printAll(arr, len);
    printf("\n");
}

// 测试 选择排序
void testEsortSelection(int *arr, int len) {
    printf("before ... \n");
    ealgorithm::ebag::printAll(arr, len);
    printf("\n");

    printf("sorting ... \n");
    ealgorithm::esort::selectionSort(arr, len);
    ealgorithm::ebag::printAll(arr, len);
    printf("\n");
}

// 测试 插入排序
void testEsortInsertion(int *arr, int len) {
    printf("before ... \n");
    ealgorithm::ebag::printAll(arr, len);
    printf("\n");

    printf("sorting ... \n");
    ealgorithm::esort::insertionSort(arr, len);
    ealgorithm::ebag::printAll(arr, len);
    printf("\n");
}

// 测试 插入排序
void testEsortShell(int *arr, int len) {
    printf("before ... \n");
    ealgorithm::ebag::printAll(arr, len);
    printf("\n");

    printf("sorting ... \n");
    ealgorithm::esort::shellSort(arr, len);
    ealgorithm::ebag::printAll(arr, len);
    printf("\n");
}


int main(int argc, char *argv[]) {
    int demoArr[] = {30, 23, 40, 34, 20, 12, 80, 78};
    int demoLen = (int) sizeof(demoArr) / sizeof(int);

    clock_t start, end;
    start = clock();

//    testEsortBubble(demoArr, demoLen);
//    testEsortSelection(demoArr, demoLen);
//    testEsortInsertion(demoArr, demoLen);
    testEsortShell(demoArr, demoLen);

    end = clock();
    double runtime = (double) (end - start) / CLOCKS_PER_SEC;
    printf("runtime %lf s \n", runtime);
    return (0);
}
