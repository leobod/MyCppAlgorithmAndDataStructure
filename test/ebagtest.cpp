/**
 * ebag的测试
 * 
 * ebag的使用案例与简单测试
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */
 
#include <iostream>

#include "../include/ebag.h"

// test ebag
void printAllTest() {
    int arr[] = {1, 2, 3, 4, 5};
    int length = 5;
    ealgorithm::ebag::printAll(arr, length);
}

int main(int argc, char *argv[]) {
    printAllTest();

    return (0);
}