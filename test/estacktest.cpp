/**
 * estack的测试
 *
 * estack的使用案例与简单测试
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */


#include <stdio.h>
#include <stdlib.h>


#include "../include/estack.h"

void sequenceStackTest() {
    ealgorithm::estack::SequenceAStack aStack;

    ealgorithm::estack::initStack(aStack);
    for (int i = 0; i < 5; ++i) {
        ealgorithm::estack::push(aStack, i);
//        aStack.data[i] = i;
//        aStack.top = i;
    }

    int recv;
    for (int i = 0; i < 5; ++i) {
        ealgorithm::estack::pop(aStack, recv);
        printf("pop the val from the stack %d \n", recv);
    }
    printf("\n");
}

void linkStackTest() {
    ealgorithm::estack::SNode *link;
    link = (ealgorithm::estack::SNode *) malloc(sizeof(ealgorithm::estack::SNode));

    ealgorithm::estack::initStack(link);

    for (int i = 0; i < 5; ++i) {
        ealgorithm::estack::push(link, i * 2);
    }

    int recv;
    for (int i = 0; i < 5; ++i) {
        ealgorithm::estack::pop(link, recv);
        printf("pop the val from the stack %d \n", recv);
    }
    printf("\n");
}

int main(int argc, char *argv[]) {
    sequenceStackTest();

    linkStackTest();

}
