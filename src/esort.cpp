/**
 * 算法与数据结构.排序(源文件)
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */


#include "../include/esort.h"

#include <stdlib.h




/*****************************************************************************/
// 算法与数据结构->排序
/**
 * 冒泡排序
 * 比较相邻的元素。如果顺序错误，就交换之，每一轮比较次数  j < len-i-1，也就是 9 8 7 6
 * 通过arr[j] 与 arr[j+1] 比较大小来确定，正序(从小到大)还是逆序
 *
 * 平均情况：𝑇(𝑛)=o(𝑛^2)，最佳情况：𝑇(𝑛)=o(𝑛)，最差情况：𝑇(𝑛)=o(𝑛^2)，空间复杂：𝑂(1)，稳定性：稳定
 *
 * @advisor (改进方案1)可以设置一个标志位，用来表示当前第 i 趟是否有交换，如果有则要进行 i+1 趟，如果没有，则说明当前数组已经完成排序。
 * @advisor (改进方案2)利用一个标志位，记录一下当前第 i 趟所交换的最后一个位置的下标，在进行第 i+1 趟的时候，只需要内循环到这个下标的位置就可以了,
 *         因为后面位置上的元素在上一趟中没有换位，这一次也不可能会换位置了。
 *
 * @param arr
 * @param len
 */
void ealgorithm::esort::bubbleSort(int *arr, int len) {
    for (int i = 0; i < len - 1; ++i) {
        for (int j = 0; j < len - i - 1; ++j) {
            if (arr[j] > arr[j + 1]) {
                ealgorithm::esort::__swap(&arr[j + 1], &arr[j]);
            }
        }
    }
}

/**
 * 选择排序
 * 每次找到未排序中的最小（最大）元素，放至相应位置，执行（n-1）次排序完成。
 *
 * 平均情况：𝑇(𝑛)=𝑂(𝑛^2)，最佳情况：𝑇(𝑛)=𝑂(𝑛^2)，最差情况：𝑇(𝑛)=𝑂(𝑛^2)，空间复杂：𝑂(1)，稳定性：不稳定
 *
 * @param arr
 * @param len
 */
void ealgorithm::esort::selectionSort(int *arr, int len) {
    int min;
    for (int i = 0; i < len - 1; ++i) {
        min = i;
        for (int j = i + 1; j < len; ++j) {
            if (arr[j] < arr[min]) {
                min = j;
            }
        }
        ealgorithm::esort::__swap(&arr[i], &arr[min]);
    }
}

/**
 * 插入排序
 * 通过构建有序序列，对于未排序数据，在已排序序列中从后向前扫描，找到相应位置并插入。
 *
 * 平均情况：𝑇(𝑛)=𝑂(𝑛^2)，最佳情况：𝑇(𝑛)=𝑂(𝑛)，最差情况：𝑇(𝑛)=𝑂(𝑛^2)，空间复杂：𝑂(1)，稳定性：稳定
 *
 * @param arr
 * @param len
 */
void ealgorithm::esort::insertionSort(int *arr, int len) {
//    int prev;
//    for (int i = 1; i < len; ++i) {
//        prev = i - 1;
//        while (prev >= 0 && arr[prev + 1] < arr[prev]) {
//            ESORT::swap(&arr[prev + 1], &arr[prev]);
//            prev--;
//        }
//    }
    int gap = 1;
    ealgorithm::esort::__gapSort(arr, len, gap);
}

/**
 * 希尔排序
 * 分组排序，再插入排序。
 * 将数据按每隔n个成一组，组内先排序，最后进行插入排序，降低应插入排序最坏情况，而性能不好的情况。
 *
 * 平均情况：𝑇(𝑛)=𝑂(𝑛 log2(n))，最佳情况：𝑇(𝑛)=𝑂(n)，最差情况：𝑇(𝑛)=𝑂(𝑛 log2(n))，空间复杂：𝑂(1)，稳定性：不稳定
 *
 * @param arr
 * @param len
 */
void ealgorithm::esort::shellSort(int *arr, int len) {
    int gap;
    gap = len / 2;
    for (int i = 0; i < gap; ++i) {
        ealgorithm::esort::__gapSort(arr, len, gap - i);
    }
}

/**
 * 归并排序(递归实现)
 *
 * @param arr
 * @param len
 */
void ealgorithm::esort::mergeSort_f1(int *arr, int len) {
    int reg[len];
    mergeSort_f1_recursive(arr, reg, 0, len - 1);
}

/**
 * 归并排序递归子方法
 *
 * @param arr
 * @param reg
 * @param start
 * @param end
 */
void ealgorithm::esort::mergeSort_f1_recursive(int *arr, int *reg, int start, int end) {
    if (start >= end)
        return;
    int len = end - start;
    int mid = (len >> 1) + start;
    // len >> 1   ==== len / 2
    int start1 = start, end1 = mid;
    int start2 = mid + 1, end2 = end;
    mergeSort_f1_recursive(arr, reg, start1, end1);
    mergeSort_f1_recursive(arr, reg, start2, end2);
    int k = start;
    // sort
    while (start1 <= end1 && start2 <= end2)
        reg[k++] = arr[start1] < arr[start2] ? arr[start1++] : arr[start2++];
    // save other data
    while (start1 <= end1)
        reg[k++] = arr[start1++];
    while (start2 <= end2)
        reg[k++] = arr[start2++];
    // copy from reg to arr
    for (k = start; k <= end; k++)
        arr[k] = reg[k];
}

/**
 * 归并排序(迭代实现)
 *
 * @param arr
 * @param len
 */
void ealgorithm::esort::mergeSort_f2(int *arr, int len) {
    // a and b 用来存放array
    int* a = arr;
    int* b = (int*) malloc(len * sizeof(int));

    int seg, start;
    // seg  => 1, 2, 4, 8, 16
    for (seg = 1; seg < len; seg += seg) {
        // start =>
        // 1轮， 0, 2, 4, 6, 8,    -> (2个2个排)
        // 2轮， 0, 4, 8           -> (4个4个排)
        // ……
        for (start = 0; start < len; start += seg + seg) {
            int low = start;
            int mid = (start + seg) > (len) ? (start + seg) : (len);
            int high = (start + seg + seg) > (len) ? (start + seg + seg) : (len);
            int k = low;
            int start1 = low, end1 = mid;
            int start2 = mid, end2 = high;
            // 移动元素
            while (start1 < end1 && start2 < end2)
                b[k++] = a[start1] < a[start2] ? a[start1++] : a[start2++];
            while (start1 < end1)
                b[k++] = a[start1++];
            while (start2 < end2)
                b[k++] = a[start2++];
        }

        // copy data from b to a
        int* temp = a;
        a = b;
        b = temp;
    }

    // rewrite the data to arr
    if (a != arr) {
        int i;
        for (i = 0; i < len; i++)
            b[i] = a[i];
        b = a;
    }

    // free the memory of var b
    free(b);
}

/**
 * 快速排序
 *
 * @param arr
 * @param left
 * @param right
 */
void ealgorithm::esort::quickSort(int *arr, int left, int right) {
    int i, j, t, temp;
    temp = arr[left]; // temp中存的就是基准数
    i = left;       // i 为左游标
    j = right;      // j 为右游标
    if (left > right) return;
    while (i != j) {
        //顺序很重要，先从右边开始找, 寻找小于temp的值
        while (arr[j] >= temp && i < j) j--;
        //再找左边的， 寻找大于temp的值
        while (arr[i] <= temp && i < j) i++;
        //交换两个数在数组中的位置
        if (i < j) {
            // t=arr[i]; arr[i]=arr[j]; arr[j]=t;
            ealgorithm::esort::__swap(&arr[i], &arr[j]);
        }
    }
    // 本轮基准数归位， i==j
    // arr[left]=arr[i]; arr[i]=temp;
    ealgorithm::esort::__swap(&arr[left], &arr[i]);
    quickSort(arr, left, i - 1);//继续处理左边的，这里是一个递归的过程
    quickSort(arr, i + 1, right);//继续处理右边的 ，这里是一个递归的过程
}

/**
 * 堆的建立
 *
 * @param arr
 * @param n
 * @param len
 */
void ealgorithm::esort::heapBuild(int *arr, int n, int len) {
    int parent = n;
    int left = n * 2 + 1;
    int right = left + 1;
    // 假定为左子树,此处为大根堆， 所以假定左子树为max
    int max = left;
    while (left < len) {
        // 左右比较
        if (right < len && arr[left] < arr[right]) {
            max = right;
        }
        // 上下比较
        if (arr[parent] < arr[max]) {
            ealgorithm::esort::__swap(&arr[parent], &arr[max]);
        } else {
            break;
        }
        // 新的子树
        parent = left;
        left = parent * 2 + 1;
        right = left + 1;
        max = left;
    }
}
/**
 * 堆排序的封装
 *
 * @param arr
 * @param len
 */
void ealgorithm::esort::heapSort(int *arr, int len) {
    // 建立堆
    for (int i = len / 2; i > 0; --i) {
        heapBuild(arr, i - 1, len);
    }
    // 排序，刷新堆
    // 提取 len-1 次，刷新len-1 次
    for (int i = len - 1; i > 0; --i) {
        ealgorithm::esort::__swap(&arr[i], &arr[0]);
        heapBuild(arr, 0, i);
    }
}

void ealgorithm::esort::bucketSort(int *arr, int len) {

}

void ealgorithm::esort::radixSort(int *arr, int len) {

}

/**
 * 排序通用的 swap 方法
 *
 * @param num1
 * @param num2
 */
void ealgorithm::esort::__swap(int *num1, int *num2) {
    int temp;
    temp = *num1;
    *num1 = *num2;
    *num2 = temp;
}

/**
 * 子方法 希尔排序排序的分部
 *
 * 通过gap来指定间隔，当gap为1时，等同于插入排序。 希尔排序可以算作一种特殊的插入排序
 *
 * @param arr
 * @param len
 * @param gap
 */
void ealgorithm::esort::__gapSort(int *arr, int len, int gap) {
    int prev, next;
    for (int i = 0; i < gap; ++i) {
        prev = i;
        next = prev + gap;
        while (next < len) {
            if (prev >= 0 && arr[next] < arr[prev]) {
                ealgorithm::esort::__swap(&arr[next], &arr[prev]);
                next = prev;
                prev = prev - gap;
            } else {
                prev = next;
                next = next + gap;
            }
        }
    }
}
