/**
 * estack的实现
 *
 * 实现顺序栈和链栈(源文件)
 *
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */


#include "../include/estack.h"

#include <stdlib.h>




/*****************************************************************************/
// [stack]顺序存储.栈
/**
 * [栈]顺序栈的初始化
 *
 * 设置top为-1
 *
 * @param S
 */
void ealgorithm::estack::initStack(ealgorithm::estack::SequenceAStack &s) {
    s.top = -1;
}

/**
 * [栈]顺序栈的是否为空
 *
 * 此处top=-1为空 top=0为第一个元素(要与顺序表的index有所区别)
 * 有时候设计人员会用top=0为空，注意区分
 *
 * @param s
 * @return
 */
bool ealgorithm::estack::stackEmpty(ealgorithm::estack::SequenceAStack &s) {
    if (s.top == -1) {
        return (true);
    }
    return (false);
}

/**
 * [栈]顺序栈的push
 *
 * (检查) 是否越界
 * 栈顶+1,并放入数据
 *
 * @param s
 * @param val
 * @return
 */
bool ealgorithm::estack::push(ealgorithm::estack::SequenceAStack &s, int val) {
    if (s.top == ealgorithm::estack::MAXZISE - 1) {
        return (false);
    }
    s.top++;
    s.data[s.top] = val;
    return (true);
}

/**
 * [栈]顺序栈的pop
 *
 * (检查) 是否为空
 * 取数据,栈顶-1,
 *
 * @param s
 * @param val
 * @return
 */
bool ealgorithm::estack::pop(ealgorithm::estack::SequenceAStack &s, int &val) {
    if (s.top < 0) {
        return (false);
    }
    val = s.data[s.top];
    s.top--;
    return (true);
}




/*****************************************************************************/
// [stack]链式存储.栈
/**
 * [stack]the function of init the link stack
 *
 * This function is the implementation of the lead node
 *
 * @param s
 */
void ealgorithm::estack::initStack(ealgorithm::estack::LinkStack &s) {
    s->next = NULL;
}

/**
 * [stack]the function of judge the stack is empty
 *
 * Judge the LinkStack is NULL
 *
 * @param s
 * @return
 */
bool ealgorithm::estack::stackEmpty(ealgorithm::estack::LinkStack &s) {
    if (s == NULL || s->next == NULL) {
        return (true);
    }
    return (false);
}

/**
 * [stack]the function of push data elem into the Stack
 *
 * @param s
 * @param val
 * @return
 */
bool ealgorithm::estack::push(ealgorithm::estack::LinkStack &s, int val) {
    ealgorithm::estack::SNode *temp;
    temp = (ealgorithm::estack::SNode *) malloc(sizeof(ealgorithm::estack::SNode));
    temp->data = val;
    temp->next = s->next;
    s->next = temp;
    return (true);
}

/**
 * [stack]the function of pop data from the stack
 *
 * @param s
 * @param val
 * @return
 */
bool ealgorithm::estack::pop(ealgorithm::estack::LinkStack &s, int &val) {
    ealgorithm::estack::SNode *temp;
    temp = s->next;
    val = temp->data;
    s->next = temp->next;
    free(temp);
    return (true);
}
