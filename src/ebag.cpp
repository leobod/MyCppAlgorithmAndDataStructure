/**
 * ebag的实现
 * 
 * 用于存放数据，并打印数据(源文件)
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */


#include "../include/ebag.h"

#include <stdio.h>


// 算法与数据结构.数组
/**
 * 打印所有数组元素
 *
 * @param arr
 * @param len
 */
void ealgorithm::ebag::printAll(int arr[], int len) {
    for (int i = 0; i < len; ++i) {
        printf("The arr[%d] = %d \n", i, arr[i]);
    }
}
