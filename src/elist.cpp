/**
 * ${TITLE}
 * 
 * Content
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */

#include "../include/elist.h"

#include <stdlib.h>




/*****************************************************************************/
// [静态数组版的]顺序表
/**
 * [静态数组版的]顺序表,初始化
 *
 * data全部设置为0
 *
 * @param l
 */
bool ealgorithm::elist::initList(ealgorithm::elist::SequenceAList &l) {
    int capacity = sizeof(l.data) / sizeof(int);
    for (int i = 0; i < capacity; ++i) {
        l.data[i] = 0;
    }
    l.length = 0;
    return (true);
}

/**
 * [静态数组版的]顺序表,插入
 *
 * (检查) 索引是否溢出，插入的元素是否超过容量
 * 在index 位置插入值为val的元素,对应的索引为index-1
 *
 * @param l
 * @param index
 * @param val
 */
bool ealgorithm::elist::listInsert(ealgorithm::elist::SequenceAList &l, int index, int val) {
    if (index < 1 || index > l.length + 1) {
        return (false);
    }
    if (l.length >= ealgorithm::elist::MAXSIZE) {
        return (false);
    }
    for (int i = l.length; i >= index; --i) {
        l.data[i] = l.data[i - 1];
    }
    l.data[index - 1] = val;
    l.length++;
    return (true);
}

/**
 * [静态数组版的]顺序表,删除
 *
 * (检查) 索引是否溢出，插入的元素是否超过容量
 * 删除index位置的元素，对应索引index-1,将值pop并存入val，
 *
 * @param l
 * @param index
 * @param val
 * @return
 */
bool ealgorithm::elist::listDelete(ealgorithm::elist::SequenceAList &l, int index, int &val) {
    if (index <1 || index > l.length) {
        return (false);
    }
    val = l.data[index - 1];
    for (int i = index; i < l.length; ++i) {
        l.data[i - 1] = l.data[i];
    }
    l.length--;
    return (true);
}

/**
 * [静态数组版的]顺序表,查找指定index元素的值
 *
 * 查找index位置的元素的值，具有随机读取特性
 *
 * @param l
 * @param index
 * @return
 */
int ealgorithm::elist::getElem(ealgorithm::elist::SequenceAList &l, int index) {
    return (l.data[index - 1]);
}

/**
 * [静态数组版的]顺序表,查找指定值的元素的index
 *
 * 查找值对应的元素的index位置
 *
 * @param l
 * @param val
 * @return
 */
int ealgorithm::elist::locateElem(ealgorithm::elist::SequenceAList &l, int val) {
    for (int i = 0; i < l.length; ++i) {
        if (l.data[i] == val) {
            return (i + 1);
        }
    }
    return (0);
}




/*****************************************************************************/
// [动态指针版的]顺序表
/**
 * 用指针与malloc来初始化顺序表
 *
 * @param l
 */
bool ealgorithm::elist::initList(ealgorithm::elist::SequencePList &l) {
    l.maxsize = ealgorithm::elist::MAXSIZE;
    l.data = (int *) malloc(sizeof(int) * l.maxsize);
    for (int i = 0; i < l.maxsize; ++i) {
        l.data[i] = 0;
    }
    l.length = 0;
    return (true);
}

/**
 * 使用malloc来实现的扩容
 *
 * @param l
 * @param len
 */
void ealgorithm::elist::increaseSize(ealgorithm::elist::SequencePList &l, int len) {
    int *p = l.data;
    l.data = (int *) malloc(sizeof(int) * (l.maxsize + len));
    for (int i = 0; i < l.length; ++i) {
        l.data[i] = p[i];
    }
    l.maxsize = l.maxsize + len;
    free(p);
}

/**
 * 【同静态】
 *
 * @param l
 * @param index
 * @param val
 */
bool ealgorithm::elist::listInsert(ealgorithm::elist::SequencePList &l, int index, int val) {
    if (index < 1 || index > l.length + 1) {
        return (false);
    }
    if (index >= l.maxsize) {
        // 或者扩容
        return (false);
    }
    for (int i = l.length; i >= index; --i) {
        l.data[i] = l.data[i - 1];
    }
    l.data[index - 1] = val;
    l.length++;
    return (true);
}

/**
 * 【同静态】
 *
 * @param l
 * @param index
 * @param val
 * @return
 */
bool ealgorithm::elist::listDelete(ealgorithm::elist::SequencePList &l, int index, int &val) {
    if (index < 1 || index > l.length) {
        return (false);
    }
    val = l.data[index - 1];
    for (int i = index; i < l.length; ++i) {
        l.data[i - 1] = l.data[i];
    }
    l.length--;
    return (true);

}

/**
 * 【同静态】
 *
 * @param l
 * @param index
 * @return
 */
int ealgorithm::elist::getElem(ealgorithm::elist::SequencePList &l, int index) {
    return (l.data[index - 1]);
}

/**
 * 【同静态】
 *
 * @param l
 * @param val
 * @return
 */
int ealgorithm::elist::locateElem(ealgorithm::elist::SequencePList &l, int val) {
    for (int i = 0; i < l.length; ++i) {
        if (l.data[i] == val) {
            return (i + 1);
        }
    }
    return (0);
}





/*****************************************************************************/
// [单链表]
/**
 * [单链表]初始化
 *
 * (无头节点) 直接L=NULL
 * (有头节点) malloc一个LNode并将L->next设置为NULL
 *
 * @param l 等于LNode *l 或者 LinkList l
 * @return
 */
bool ealgorithm::elist::initList(ealgorithm::elist::LinkList &l) {
    l = NULL;
    return (true);
}

bool ealgorithm::elist::initListHasHead(ealgorithm::elist::LinkList &l) {
    l = (LNode *) malloc(sizeof(LNode));
    if (l == NULL) {
        return (false);
    }
    l->next = NULL;
    return (true);
}

/**
 * [单链表]是否为空
 *
 * (无头节点) 判定L
 * (有头节点) 判定L->next
 *
 * @param l
 * @param withHead
 * @return
 */
bool ealgorithm::elist::empty(ealgorithm::elist::LinkList &l) {
    if (l == NULL) {
        return (true);
    }
    return (false);
}
bool ealgorithm::elist::emptyHasHead(ealgorithm::elist::LinkList &l) {
    if (l->next == NULL) {
        return (true);
    }
    return (false);
}

/**
 * [单链表]位序插入
 *
 * (p:游标) 遍历LinkList
 * (current:游标对应的位置) 对于无头节点的从1开始,对于有头节点的从0开始
 * (检查) 元素是否存在，索引是否溢出
 * (无头节点) 比有头节点的多一个index=1的特殊处理, 若index!=1，则current=1继续,在进入while
 * (有头节点) 检查符合时，current=0开始,在进入while
 *
 * @param l
 * @param index
 * @param val
 * @param withHead
 * @return
 */
bool ealgorithm::elist::listInsert(ealgorithm::elist::LinkList &l, int index, int val) {
    if (index < 1) {
        return (false);
    }
    ealgorithm::elist::LNode *p = l, *s;
    if (index == 1) {
        s = (ealgorithm::elist::LNode *) malloc(sizeof(ealgorithm::elist::LNode));
        s->data = val;
        s->next = l;
        l = s;
        return (true);
    }
    int current = 1;
    while (p != NULL && current < index - 1) {
        p = p->next;
        current++;
    }
    if (p == NULL) {
        return (false);
    }
    s = (ealgorithm::elist::LNode *) malloc(sizeof(ealgorithm::elist::LNode));
    s->data = val;
    s->next = p->next;
    p->next = s;
    return (true);
}
bool ealgorithm::elist::listInsertHasHead(ealgorithm::elist::LinkList &l, int index, int val) {
    if (index < 1) {
        return (false);
    }
    ealgorithm::elist::LNode *p = l, *s;
    int current = 0;
    while (p != NULL && current < index - 1) {
        p = p->next;
        current++;
    }
    if (p == NULL) {
        return (false);
    }
    s = (ealgorithm::elist::LNode *) malloc(sizeof(ealgorithm::elist::LNode));
    s->data = val;
    s->next = p->next;
    // 容易写成 s->next = NULL; 忽视来部分细节
    p->next = s;
    return (true);
//    ealgorithm::elist::__insertNextNode(p, val);
}

/**
 * [单链表]位序删除
 *
 * (p:游标) 遍历LinkList
 * (current:游标对应的位置) 对于无头节点的从1开始,对于有头节点的从0开始
 * (检查) 元素是否存在，索引是否溢出
 * (无头节点) 比有头节点的多一个index=1的特殊处理, 若index!=1，则current=1继续,在进入while
 * (有头节点) 检查符合时，current=0开始,在进入while
 * 对于取到的元素,pop,并释放LNode内存空间
 *
 * @param l
 * @param index
 * @param val
 * @param withHead
 * @return
 */
bool ealgorithm::elist::listDelete(ealgorithm::elist::LinkList &l, int index, int &val) {
    if (index < 1) {
        return (false);
    }
    LNode *p = l, *q;
    if (index == 1) {
        q = l;
        val = q->data;
        l = q->next;
        free(q);
        return (true);
    }
    int current = 1;
    while (p != NULL && current < index - 1) {
        p = p->next;
        current++;
    }
    if (p == NULL || p->next == NULL) {
        return (false);
    }
    q = p->next;
    val = q->data;
    p->next = q->next;
    free(q);
    return (true);
}
bool ealgorithm::elist::listDeleteHasHead(ealgorithm::elist::LinkList &l, int index, int &val) {
    if (index < 1) {
        return (false);
    }
    LNode *p = l;
    int current = 0;
    while (p != NULL && current < index - 1) {
        p = p->next;
        current++;
    }
    if (p == NULL || p->next == NULL) {
        return (false);
    }
    LNode *q = p->next;
    val = q->data;
    p->next = q->next;
    free(q);
    return (true);
}

/**
 * [单链表]指定节点后插入
 *
 * (检查) p是否为空
 * 如果不为空，则新建LNode节点s
 * (后插法) 先用s节点存数据，并用s->next=p->next，并将p->next指向s
 *
 * @param p
 * @param val
 * @return
 */
bool ealgorithm::elist::__insertNextNode(ealgorithm::elist::LNode *p, int val) {
    if (p == NULL) {
        return (false);
    }
    ealgorithm::elist::LNode *s;
    s = (ealgorithm::elist::LNode *) malloc(sizeof(ealgorithm::elist::LNode));
    // 后插法，先用s节点存数据，并用s->next=p->next，并将p->next指向s
    s->data = val;
    s->next = p->next;
    p->next = s;
    return (true);
}

/**
 * [单链表]指定节点前面插入
 *
 * (检查) p是否为空
 * 如果不为空，则新建LNode节点s
 * (前插法) 先用s节点复制节点p的数据，并将p->next指向s，再用p存新的数据
 *
 * @param p
 * @param val
 * @return
 */
bool ealgorithm::elist::__insertBeforeNode(ealgorithm::elist::LNode *p, int val) {
    if (p == NULL) {
        return (false);
    }
    ealgorithm::elist::LNode *s;
    s = (ealgorithm::elist::LNode *) malloc(sizeof(ealgorithm::elist::LNode));
    if (s == NULL) {
        return (false);
    }
    // 前插法，先用s节点复制节点p的数据，并将p->next指向s，再用p存新的数据
    s->next = p->next;
    s->data = p->data;
    p->next = s;
    p->data = val;
    return (true);
}

/**
 * [单链表]位序查找
 *
 * (p:游标) 遍历LinkList
 * (current:游标对应的位置) 对于无头节点的从1开始,对于有头节点的从0开始
 * (无头节点) 开始时 p=l current=1
 * (有头节点) 开始时 p=l current=0    p=p->next current=1    其中current=0特殊的表示为头节点
 *
 * @param l
 * @param index
 * @return
 */
ealgorithm::elist::LNode *ealgorithm::elist::getElem(ealgorithm::elist::LinkList &l, int index) {
    if (index < 1) {
        return (NULL);
    }
    ealgorithm::elist::LNode *p = l;
    int current = 1;
    while (p != NULL && current < index) {
        p = p->next;
        current++;
    }
    return (p);
}
ealgorithm::elist::LNode *ealgorithm::elist::getElemHasHead(ealgorithm::elist::LinkList &l, int index) {
    if (index < 0) {
        return (NULL);
    }
    ealgorithm::elist::LNode *p = l;
    int current = 0;
    while (p != NULL && current < index) {
        p = p->next;
        current++;
    }
    return (p);
}




/*****************************************************************************/
// [双链表]
/**
 * [双链表]初始化
 *
 * malloc一个DNode，并设置prior和next为NULL
 *
 * @param l
 * @return
 */
bool ealgorithm::elist::initDList(ealgorithm::elist::DLinkList &l) {
    l = (ealgorithm::elist::DNode *) malloc(sizeof(ealgorithm::elist::DNode));
    if (l == NULL) {
        return (false);
    }
    l->prior = NULL;
    l->next = NULL;
    return (true);
}

/**
 * [双链表]后插入
 *
 * 先设置s,让其风别指向p->next与p
 * 在设置p原来下一个节点的prior指向s(此处为容易遗漏的地方)
 * 最后设置p->next的指向
 *
 * @param p
 * @param s
 * @return
 */
bool ealgorithm::elist::__insertNextDNode(ealgorithm::elist::DNode *p, ealgorithm::elist::DNode *s) {
    if (p == NULL || s == NULL) {
        return (false);
    }
    s->next = p->next;
    s->prior = p;
    // 下面的容易遗漏
    if (p->next != NULL) {
        p->next->prior = s;
    }
    p->next = s;
    return (true);
}

/**
 * [双链表]删除节点
 *
 * (检查) p是否为空，q是否为空，q->next是否存在
 *
 * @param p
 * @return
 */
bool ealgorithm::elist::deleteDNode(ealgorithm::elist::DNode *p) {
    if (p == NULL) {
        return (false);
    }
    ealgorithm::elist::DNode *q = p->next;
    if (q == NULL) {
        return (false);
    }
    p->next = q->next;
    if (q->next != NULL) {
        q->next->prior = p;
    }
    free(q);
    return (true);
}
