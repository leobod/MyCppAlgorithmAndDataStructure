/**
 * ${TITLE}
 * 
 * Content
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */


#include "../include/equeue.h"

#include <stdlib.h>




/*****************************************************************************/
// [queue]顺序存储.队列
/**
 * [queue]the function of init the sequence queue
 *
 * @param q
 * @return
 */
bool ealgorithm::equeue::initQueue(ealgorithm::equeue::SequenceQueue &q) {
    q.front = 0;
    q.rear = 0;
    return (true);
}

/**
 * [queue]the function of judge the queue
 *
 * @param q
 * @return
 */
bool ealgorithm::equeue::queueEmpty(ealgorithm::equeue::SequenceQueue &q) {
    // Don't add too many distractions, such as maxsize
    // Such as, (q.front % MAXSIZE == q.rear %MAXSIZE)
    if (q.front == q.rear) {
        return (true);
    }
    return (false);
}

/**
 * [queue]the function of enter the queue
 *
 * @param q
 * @param val
 * @return
 */
bool ealgorithm::equeue::enQueue(ealgorithm::equeue::SequenceQueue &q, int val) {
    if ((q.front % ealgorithm::equeue::MAXSIZE) == ((q.rear + 1) % ealgorithm::equeue::MAXSIZE)) {
        return (false);
    }
    q.data[q.rear] = val;
    q.rear = (q.rear + 1) % ealgorithm::equeue::MAXSIZE;
    return (true);
}

/**
 * [queue]the function of get out of the queue
 *
 * @param q
 * @param val
 * @return
 */
bool ealgorithm::equeue::deQueue(ealgorithm::equeue::SequenceQueue &q, int &val) {
    if (ealgorithm::equeue::queueEmpty(q)) {
        return (false);
    }
    val = q.data[q.front];
    q.front = (q.front + 1) % ealgorithm::equeue::MAXSIZE;
    return (true);
}




/*****************************************************************************/
// [queue]链式存储.队列
/**
 * [queue]the function of init link queue
 *
 * @param q
 * @return
 */
bool ealgorithm::equeue::initQueue(ealgorithm::equeue::LinkQueue &q) {
    ealgorithm::equeue::LinkNode *temp;
    temp = (ealgorithm::equeue::LinkNode *) malloc(sizeof(ealgorithm::equeue::LinkNode));
    if (temp == NULL) {
        return (false);
    }
    temp->next = NULL;
    q->front = q->rear = temp;
}

/**
 * [queue]the function of judge the queue is empty
 *
 * @param q
 * @return
 */
bool ealgorithm::equeue::queueEmpty(ealgorithm::equeue::LinkQueue &q) {
    if (q->front == q->rear) {
        return (true);
    }
    return (false);
}

/**
 * [queue]LinkQueue enQueue
 *
 * @param q
 * @param val
 * @return
 */
bool ealgorithm::equeue::enQueue(ealgorithm::equeue::LinkQueue &q, int val) {
    ealgorithm::equeue::LinkNode *temp;
    temp = (ealgorithm::equeue::LinkNode *) malloc(sizeof(ealgorithm::equeue::LinkNode));
    temp->data = val;
    // 或者可以直接 temp->next = NULL;
    temp->next = q->rear->next;
    // 下面2条不要遗漏
    q->rear->next = temp;
    q->rear = temp;
    return (true);
}

/**
 * [queue]LinkQueue.deQueue
 *
 * @param q
 * @param val
 * @return
 */
bool ealgorithm::equeue::deQueue(ealgorithm::equeue::LinkQueue &q, int &val) {
    if (q->front == q->rear) {
        return (false);
    }
    // 需要再理解
    ealgorithm::equeue::LinkNode *temp;
    temp = q->front->next;
    val = temp->data;
    q->front->next = temp->next;
    // 尤其不能遗漏这个语句
    if (q->rear == temp) {
        q->front = q->rear;
    }
    free(temp);
    return (true);
}

