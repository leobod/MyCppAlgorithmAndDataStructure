/**
 * ${TITLE}
 * 
 * Content
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */

#include "../include/etree.h"

#include <stdio.h>
#include <stdlib.h>




/*****************************************************************************/
// [tree]二叉树
/**
 * [tree]init the binary tree
 *
 * @param btree
 * @return
 */
bool ealgorithm::etree::initTree(ealgorithm::etree::BiTree &btree) {
    btree->lchild = NULL;
    btree->rchild = NULL;
}

/**
 * [tree]calculate the depth of the binary tree
 *
 * @param btree
 * @return
 */
int ealgorithm::etree::treeDepth(ealgorithm::etree::BiTree &btree) {
    int l, r;
    if (btree == NULL) {
        return 0;
    } else {
        l = treeDepth(btree->lchild);
        r = treeDepth(btree->rchild);
        return l > r ? l + 1 : r + 1;
    }
}

/**
 * [tree]print the data of visit the data of the binary tree
 *
 * @param btree
 */
void ealgorithm::etree::visit(ealgorithm::etree::BiTree &btree) {
    if (btree != NULL) {
        printf("值为: %d \t", btree->data);
    }
}

/**
 * [tree]前序遍历
 *
 * @param btree
 */
void ealgorithm::etree::preOrder(ealgorithm::etree::BiTree &btree) {
    if (btree != NULL) {
        visit(btree);
        preOrder(btree->lchild);
        preOrder(btree->rchild);
    }
}

/**
 * [tree]中序遍历
 *
 * @param btree
 */
void ealgorithm::etree::inOrder(ealgorithm::etree::BiTree &btree) {
    if (btree != NULL) {
        inOrder(btree->lchild);
        visit(btree);
        inOrder(btree->rchild);
    }

}

/**
 * [tree]后序遍历
 *
 * @param btree
 */
void ealgorithm::etree::postOrder(ealgorithm::etree::BiTree &btree) {
    // 分清楚递归, 这儿不是while， 用while 死循环了
    if (btree != NULL) {
        postOrder(btree->lchild);
        postOrder(btree->rchild);
        visit(btree);
    }
}




/*****************************************************************************/
// [tree]线索二叉树
/**
 * [tree]init the clues binary tree
 *
 * @param tree
 * @return
 */
bool ealgorithm::etree::initTree(ealgorithm::etree::CluesBTree &tree) {
    return false;
}

bool ealgorithm::etree::find(ealgorithm::etree::CluesBTree &tree) {
    return false;
}

/**
 * [tree]前序线索化
 *
 * @param tree
 */
void ealgorithm::etree::preThread(ealgorithm::etree::CluesBTree &tree, ealgorithm::etree::CluesBTree &pre) {
    if (tree != NULL) {
        visit(tree, pre);
        preThread(tree->lchild, pre);
        preThread(tree->rchild, pre);
    }
}

/**
 * [tree]中序线索化
 *
 * @param tree
 */
void ealgorithm::etree::inThread(ealgorithm::etree::CluesBTree &tree, ealgorithm::etree::CluesBTree &pre) {
    if (tree != NULL) {
        inThread(tree->lchild, pre);
        // 2个if 和一个赋值是关键，封装请看 visit(p, q)
        if (tree->lchild == NULL) {
            tree->lchild = pre;
            tree->ltag = 1;
        }
        if (pre != NULL && pre->rchild == NULL) {
            pre->rchild = tree;
            pre->rtag = 1;
        }
        pre = tree;
        inThread(tree->rchild, pre);
    }
}

/**
 * [tree]后序线索化
 *
 * @param tree
 */
void ealgorithm::etree::postThread(ealgorithm::etree::CluesBTree &tree, ealgorithm::etree::CluesBTree &pre) {
    if (tree != NULL) {
        postThread(tree->lchild, pre);
        postThread(tree->rchild, pre);
        visit(tree, pre);
    }

}

void ealgorithm::etree::visit(ealgorithm::etree::CluesBTree &p, ealgorithm::etree::CluesBTree &q) {
    if (p->lchild == NULL) {
        p->lchild = q;
        p->ltag = 1;
    }
    if (q != NULL && q->rchild == NULL) {
        q->rchild = p;
        q->rtag = 1;
    }
    q = p;
}
