/**
 * ealgorithm 的实现
 * 
 * 对于基本数据结构与算法的描述(源文件)
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */
 

#include "../include/ealgorithm.h"
