/**
 * ${TITLE}
 * 
 * Content
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */


#include "../include/esearch.h"




/*****************************************************************************/
// 算法与数据结构->查找
/**
 * [search]顺序查找
 *
 * @param arr
 * @param len
 * @param val
 * @return index 返回索引值(从0开始)
 */
int ealgorithm::esearch::sequenceSearch(int *arr, int len, int val) {
    for (int i = 0; i < len; ++i) {
        if (arr[i] == val) {
            return (i);
        } else {
            return (-1);
        }
    }
}

/**
 * [search]使用循环来实现二分查找
 *
 * (说明) 有序查找
 *
 * @param arr
 * @param len
 * @param val
 * @return
 */
int ealgorithm::esearch::binarySearch(int *arr, int len, int val) {
    int low, high, mid;
    low = 0;
    high = len - 1;
    // 分清什么时候while 什么时候+1 -1
    while(low<=high) {
        mid = (low+high)/2;
        if (arr[mid] == val) {
            return (mid);
        }
        if (arr[mid] > val) {
            high = mid--;
        }
        if (arr[mid] < val) {
            low = mid++;
        }
    }
    return (-1);
}

/**
 * [search]递归实现二分查找
 *
 * (说明) 有序查找
 *
 * @param arr
 * @param low
 * @param high
 * @param val
 * @return
 */
int ealgorithm::esearch::binarySearch2(int *arr, int low, int high, int val) {
    int mid = low+(high-low)/2;

    if (arr[mid] == val) {
        return (mid);
    }
    if (arr[mid] > val) {
        high = mid--;
        return binarySearch2(arr, low, high, val);
    }
    if (arr[mid] < val) {
        low = mid++;
        return binarySearch2(arr, low, high, val);
    }
    return (-1);
}

int ealgorithm::esearch::chunkSearch(int *arr) {
    return 0;
}

int ealgorithm::esearch::hashSearch(int *arr) {
    return 0;
}
