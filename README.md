# MyCppAlgorithmAndDataStructure

#### 介绍
To realize the algorithm and data structure with Cpp

 ![1](https://img.shields.io/badge/license-Apache%202.0-blue.svg)

 ![ESIDE.CN](https://img.shields.io/badge/CN-ESIDE-orange.svg?longCache=true)

 ![v1.0.0](https://img.shields.io/badge/version-v1.0.0-red.svg?longCache=true)


#### 软件架构
软件架构说明
+ include : 用于放置头文件`.h`
+ src : 用于放置头文件对应的源文件实现`.cpp`
+ CMakeLists.txt : 是CMake的设置文件

#### 安装教程

```
用户在切换到当前目录的情况下，可以在自己的设备上进行cmake。
cmake .

cmake过后,使用make来编译
make
```

#### 使用说明

ealgorithm中包含了所有的的模块

#### 参与贡献

发送邮件到: join@eside.cn

#### 补充

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  LEOBOD 的个人主页。 [www.leobod.cn](https://www.leobod.cn)
3.  ESIDE.CN 欢迎所有志同道合的伙伴们一起交流。 [www.eside.cn](https://www.eside.cn)

