/**
 * ebag
 * 
 * 用于存放数据，并打印数据
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */

#ifndef DEMOCPP_EBAG_H
#define DEMOCPP_EBAG_H


// 算法与数据结构
namespace ealgorithm {

    // 算法与数据结构.数组
    namespace ebag {
        // 打印数组所有的值
        void printAll(int arr[], int len);
    }

}


#endif //DEMOCPP_EBAG_H
