/**
 * ${TITLE}
 * 
 * Content
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */

#ifndef DEMOCPP_ESEARCH_H
#define DEMOCPP_ESEARCH_H


// 算法与数据结构
namespace ealgorithm {

    // 算法与数据结构.查找
    namespace esearch {
        // 顺序查找
        int sequenceSearch(int arr[], int len, int val);

        // 折半查找
        int binarySearch(int arr[], int len, int val);
        int binarySearch2(int arr[], int low, int high, int val);

        // 分块查找
        int chunkSearch(int *arr);

        // hash查找
        int hashSearch(int *arr);

        // 二叉查找树
        int binary_treeSearch(int *arr);

        // 平衡查找树 2-3树
        int balance_treeSearch(int *arr);

        // 红黑树查找
        int redblack_treeSearch(int *arr);

        // B树与B+树查找
        int b_treeSearch(int *arr);
        int bp_treeSearch(int *arr);
    }

}


#endif //DEMOCPP_ESEARCH_H
