/**
 * ${TITLE}
 * 
 * Content
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */

#ifndef DEMOCPP_ETREE_H
#define DEMOCPP_ETREE_H


// 算法与数据结构
namespace ealgorithm {

    // 算法与数据结构.数
    namespace etree {

        // [tree]the structure of binary tree
        struct bNode {
            int data;
            struct bNode *lchild, *rchild;
        };
        typedef struct bNode BitNode;
        typedef struct bNode *BiTree;

        // [tree]the function of init the binary tree
        bool initTree(BiTree &btree);

        // [tree]the function of calculate the depth of the binary tree
        int treeDepth(BiTree &btree);

        // [tree]the function of print the data of the binary tree
        void visit(BiTree &btree);

        // [tree]the function of pre order the tree
        void preOrder(BiTree &btree);

        // [tree]the function of in order the tree
        void inOrder(BiTree &btree);

        // [tree]the function of post order the tree
        void postOrder(BiTree &btree);


        // [tree]the structure of clues binary tree
        struct cbNode {
            int data;
            int ltag, rtag;
            struct cbNode *lchild, *rchild;
        };
        typedef struct cbNode CluesBNode;
        typedef struct cbNode *CluesBTree;

        // [tree]the function of init the clues binary tree
        bool initTree(CluesBTree &tree);

        // [tree]the function of find the pre and next
        bool find(CluesBTree &tree);

        // [tree]the function of pre thread the clues binary tree
        void preThread(CluesBTree &tree, CluesBTree &pre);

        // [tree]the function of in thread the clues binary tree
        void inThread(CluesBTree &tree, CluesBTree &pre);

        // [tree]the function of post thread the clues binary tree
        void postThread(CluesBTree &tree, CluesBTree &pre);

        void visit(CluesBTree &p, CluesBTree &q);
    }
}

#endif //DEMOCPP_ETREE_H
