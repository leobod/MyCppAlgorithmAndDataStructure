/**
 * estack
 *
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */

#ifndef DEMOCPP_ESTACK_H
#define DEMOCPP_ESTACK_H


// 算法与数据结构
namespace ealgorithm {

    // 算法与数据结构.栈
    namespace estack {
        // 设置容量最大值
        const int MAXZISE = 10;

        // [栈]顺序栈
        typedef struct {
            int data[MAXZISE];
            int top;
        }SequenceAStack;

        // [栈]顺序栈的初始化
        void initStack(SequenceAStack &s);

        // [栈]顺序栈的是否为空
        bool stackEmpty(SequenceAStack &s);

        // [栈]顺序栈的push
        bool push(SequenceAStack &s, int val);

        // [栈]顺序栈的pop
        bool pop(SequenceAStack &s, int &val);

        // [栈]链栈
        typedef struct SNode {
            int data;
            struct SNode *next;
        }SNode, *LinkStack;

        // [stack]the function of init LinkStack
        void initStack(LinkStack &s);

        // [stack]the function of judge the stack is empty
        bool stackEmpty(LinkStack &s);

        // [stack]the function of push data elem to the link stack
        bool push(LinkStack &s, int val);

        // [stack]the function of pop data elem from the link stack
        bool pop(LinkStack &s, int &val);

    }
}

#endif //DEMOCPP_ESTACK_H
