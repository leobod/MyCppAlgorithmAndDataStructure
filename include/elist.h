/**
 * ${TITLE}
 * 
 * Content
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */

#ifndef DEMOCPP_ELIST_H
#define DEMOCPP_ELIST_H


// 算法与数据结构
namespace ealgorithm {

    // 算法与数据结构.顺序表.链表
    namespace elist {
        // 设置容量最大值
        const int MAXSIZE = 10;

        // [静态数组版的]顺序表,数据结构
        typedef struct {
            int data[MAXSIZE];
            int length;
        }SequenceAList;

        // [静态数组版的]顺序表,初始化
        bool initList(SequenceAList &l);

        // [静态数组版的]顺序表,插入
        bool listInsert(SequenceAList &l, int index, int val);

        // [静态数组版的]顺序表,删除
        bool listDelete(SequenceAList &l, int index, int &val);

        // [静态数组版的]顺序表,查找指定index元素的值
        int getElem(SequenceAList &l, int index);

        // [静态数组版的]顺序表,查找指定值的元素的index
        int locateElem(SequenceAList &l, int val);


        // [动态指针版的]顺序表.数据结构
        typedef struct {
            int *data;
            int maxsize;
            int length;
        } SequencePList;

        // [动态指针版的]顺序表,初始化
        bool initList(SequencePList &l);

        // [动态指针版的]顺序表,扩容
        void increaseSize(SequencePList &l, int len);

        // [动态指针版的]顺序表,插入
        bool listInsert(SequencePList &l, int index, int val);

        // [动态指针版的]顺序表,删除
        bool listDelete(SequencePList &l, int index, int &val);

        // [动态指针版的]顺序表,查找指定index元素的值
        int getElem(SequencePList &l, int index);

        // [动态指针版的]顺序表,查找指定值的元素的index
        int locateElem(SequencePList &l, int val);


        // [单链表]数据结构
        typedef struct LNode {
            int data;
            struct LNode *next;
        }LNode, *LinkList;

        // [单链表]初始化
        bool initList(LinkList &l);
        bool initListHasHead(LinkList &l);

        // [单链表]是否为空
        bool empty(LinkList &l);
        bool emptyHasHead(LinkList &l);

        // [单链表]位序插入
        bool listInsert(LinkList &l, int index, int val);
        bool listInsertHasHead(LinkList &l, int index, int val);

        // [单链表]位序删除
        bool listDelete(LinkList &l, int index, int &val);
        bool listDeleteHasHead(LinkList &l, int index, int &val);

        // [单链表]指定节点后插入
        bool __insertNextNode(LNode *p, int val);

        // [单链表]指定节点前面插入
        bool __insertBeforeNode(LNode *p, int val);

        // [单链表]位序查找
        LNode *getElem(LinkList &l, int index);
        LNode *getElemHasHead(LinkList &l, int index);


        // [双链表]数据结构
        typedef struct DNode {
            int data;
            struct DNode *prior, *next;
        }DNode, *DLinkList;

        // [双链表]初始化
        bool initDList(DLinkList &l);

        // [双链表]后插入
        bool __insertNextDNode(DNode *p, DNode *s);

        // [双链表]删除节点
        bool deleteDNode(DNode *p);

    }

}


#endif //DEMOCPP_ELIST_H
