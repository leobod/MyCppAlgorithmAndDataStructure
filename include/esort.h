/**
 * 算法与数据结构.排序
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */

#ifndef DEMOCPP_ESORT_H
#define DEMOCPP_ESORT_H


// 算法与数据结构
namespace ealgorithm {

    // 算法与数据结构.排序
    namespace esort {
        // 冒泡排序
        void bubbleSort(int *arr, int len);

        // 选择排序
        void selectionSort(int *arr, int len);

        // 插入排序
        void insertionSort(int *arr, int len);

        // 希尔排序
        void shellSort(int *arr, int len);

        // 归并排序 (f1 为递归实现， f2为迭代)
        void mergeSort_f1(int *arr, int len);
        void mergeSort_f1_recursive(int *arr, int *reg, int start, int end);
        void mergeSort_f2(int *arr, int len);

        // 快速排序
        void quickSort(int *arr, int left, int right);

        // 堆排序
        void heapBuild(int *arr, int n, int len);
        void heapSort(int *arr, int len);

        // 桶排序
        void bucketSort(int *arr, int len);

        // 基数排序
        void radixSort(int *arr, int len);

        // swap
        void __swap(int *num1, int *num2);

        // insert sort with gap ( dynamic shell sort )
        void __gapSort(int *arr, int len, int gap);

    }

}


#endif //DEMOCPP_ESORT_H
