/**
 * ealgorithm 的定义
 *
 * 对于基本数据结构与算法的描述(头文件)
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */

#ifndef DEMOCPP_EALGORITHM_H
#define DEMOCPP_EALGORITHM_H

#include "ebag.h"
#include "elist.h"
#include "estack.h"
#include "equeue.h"
#include "esearch.h"
#include "esort.h"
#include "etree.h"

// 算法与数据结构
namespace ealgorithm {

}


#endif //DEMOCPP_EALGORITHM_H
