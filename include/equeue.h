/**
 * ${TITLE}
 * 
 * Content
 * 
 * @author leobod(leobod@eside.cn)
 * @version v1.0.0
 * 
 * @license Apache License v2.0
 * @copyright (C) ESIDE.CN 
 */

#ifndef DEMOCPP_EQUEUE_H
#define DEMOCPP_EQUEUE_H


// 算法与数据结构
namespace ealgorithm {

    // 算法与数据结构.队列
    namespace equeue {

        // 设置容量最大值
        const int MAXSIZE = 10;

        // [queue]the structure of the sequence queue
        typedef struct {
            int data[MAXSIZE];
            int front, rear;
        } SequenceQueue;

        // [queue]the function of init the sequence queue
        bool initQueue(SequenceQueue &q);

        // [queue]the function of judge the queue is empty
        bool queueEmpty(SequenceQueue &q);

        // [queue]the function of enter the queue
        bool enQueue(SequenceQueue &q, int val);

        // [queue]the function of get out of the queue
        bool deQueue(SequenceQueue &q, int &val);


//        // [queue]the structure of the sequence queue
//        typedef struct {
//            int data[MAXSIZE];
//            int front, rear;
//            int size;
//        } SequenceQueue_S;


        // [queue]the structure of the link queue
        struct LNode {
            int data;
            struct LNode *next;
        };
        typedef struct LNode LinkNode;
        typedef struct {
            LinkNode *front, *rear;
        } linkedQueue, *LinkQueue;
        
        // [queue]the function of init the link queue
        bool initQueue(LinkQueue &q);

        // [queue]the function of judge the queue is empty
        bool queueEmpty(LinkQueue &q);

        // [queue]LinkQueue enQueue
        bool enQueue(LinkQueue &q, int val);

        // [queue]LinkQueue deQueue
        bool deQueue(LinkQueue &q, int &val);

    }

}

#endif //DEMOCPP_EQUEUE_H
